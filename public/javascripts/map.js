 var map = L.map('main_map').setView([-28.6369656,-65.1334097], 15);

 L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
     attribution: '&copy: <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
 }).addTo(map);

 //L.marker([-28.6286718,-65.1396324]).addTo(map);

 $.ajax({
     dataType: "json",
     url: "api/bicicletas",
     success: function(result){
         console.log(result),
         result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
         })
     }
 })