var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

const base_url = 'http://localhost:3000/api/bicicletas';

const aBici = { 
 code: 7, 
 color: "azul", 
 modelo: "montaña", 
 lat: -28, 
 long:-65
};


describe("Bicicleta API", () => {
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database!');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });
    });

    describe("GET BICICLETAS /", () => {
        it('Status 200', (done) => {

            request.get(base_url, function(error, response, body) {
                var result = JSON.parse(body);
                console.log('funcionando');
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });
    
    describe("POST BICICLETAS /create", () => {
        it("Status 200", (done) => {
            var headers = {'content-type' : 'application/json'};
            //var aBici = '{ "code": 7, "color": "azul", "modelo": "montaña", "lat": -28, "long":-65}';
            request.post({
                headers: headers,
                body: JSON.stringify(aBici),
                url: `${base_url}/create`,                
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe(aBici.color);
                expect(bici.ubicacion[0]).toBe(aBici.lat);
                expect(bici.ubicacion[1]).toBe(aBici.long);
                done();
            });      
        });
    });    
});


/* describe('Bicicleta API', () => {
    describe('GET BICICLETAS /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'verde', 'urbana', [-28.6358953,-65.1262138]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id": 7, "color": "azul", "modelo": "montaña", "lat": -28, "long": -65}';
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(7).color).toBe("azul");
                done();
            });
        });
    });

    describe('DELETE BICICLETAS /delete', () => {
        it('Stauts 204', (done) => {
            
        })
    })
}); */